**Pipeline Status**  [![pipeline status](https://gitlab.com/ICK/its-interview/badges/master/pipeline.svg)](https://gitlab.com/ICK/its-interview/-/commits/master) 



#### Downloading the application

You can get the application with 2 ways

- Downloading jar from gitlab pipelines page (artifacts sections)

  ![](assets/img.png)

  

- Pulling the latest image in docker hub

  ![](assets/img_1.png)

  

Project variables

`api.siret.base-url`: Base url for open data ws dedicated to companies
`api.siret.token`: Access token
`application.companies`: List of siret codes
`application.file.path`: Destination folder
`application.file.name`: File name

#### Before launching application

Make sure that you have the correct token and the url https://api.insee.fr/entreprises/sirene/V3 is still valid.

Change property `application.file.path` path to match your csv file destination folder

#### Start Application

If you have jar file you can use the following command: java -jar interview.jar, you can also override properties using `-Dpropertyname` eg to change the token use `java -jar -Dapi.siret.token=AAAAAAA interview.jar`

In case you have docker you can pull the following image  `ibrahimchafik/interview:latest`. By default the application expose port 8080

#### Exposed Api

```
`GET BASE_URL/company/siret/sync`
```

Get informations of all SIRET defined in configuration file (`application.companies` variable) and export them into csv file located in folder defined in variable `application.file.path`