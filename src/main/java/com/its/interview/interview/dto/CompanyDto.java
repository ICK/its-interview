package com.its.interview.interview.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName("etablissement")
public class CompanyDto {
    private String siret;
    private String nic;
    private String dateCreationEtablissement;
    private String denominationUniteLegale;

    public String[] toArray() {
        return new String[]{siret, nic, dateCreationEtablissement, denominationUniteLegale};
    }
}
