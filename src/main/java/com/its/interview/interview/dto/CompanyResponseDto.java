package com.its.interview.interview.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyResponseDto {
    private HeaderDto header;
    private CompanyDto etablissement;
}
