package com.its.interview.interview.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeaderDto {
    private String message;
    private Long statut;
}
