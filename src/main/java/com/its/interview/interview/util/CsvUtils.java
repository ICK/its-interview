package com.its.interview.interview.util;

import com.its.interview.interview.dto.CompanyDto;
import com.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Slf4j
@Component
public class CsvUtils {

    @Value("${application.file.name}")
    private String filename;
    @Value("${application.file.path}")
    private String filepath;

    public void write(List<CompanyDto> companies) {
        try (CSVWriter writer = new CSVWriter(new FileWriter(filepath + filename))) {
            writer.writeNext(new String[]{"siret", "nic", "dateCreationEtablissement", "denominationUniteLegale"});
            companies.forEach(c -> {
                if (c != null) {
                    writer.writeNext(c.toArray());
                }
            });
        } catch (IOException e) {
            log.error("An error occurred while writing csv file", e);
        }
    }
}
