package com.its.interview.interview.service;

import com.its.interview.interview.client.CompanyRestClient;
import com.its.interview.interview.dto.CompanyDto;
import com.its.interview.interview.dto.CompanyResponseDto;
import com.its.interview.interview.util.CsvUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class CompanyService {

    @Value("${api.siret.token}")
    private String token;
    @Autowired
    private CompanyRestClient companyRestClient;
    @Autowired
    private CsvUtils csvUtils;

    public List<CompanyDto> findBySiretNumbers(List<String> siretNumber) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + token);
        List<CompanyDto> companies = new ArrayList<>();
        for (String s : siretNumber) {
            log.info("Finding information of {}", s);
            companies.add(companyRestClient.findBySiret(s, headers).getEtablissement());
        }
        return companies;
    }

    public void syncCompanies(List<String> sirets) {
        List<CompanyDto> companies = findBySiretNumbers(sirets);
        if (CollectionUtils.isEmpty(companies)) {
            log.warn("No information available for {}", sirets);
        } else {
            csvUtils.write(companies);
        }
    }
}
