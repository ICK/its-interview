package com.its.interview.interview.controller;

import com.its.interview.interview.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("company")
public class CompanyController {

    @Value("${application.companies}")
    List<String> sirets;
    @Autowired
    private CompanyService companyService;

    @GetMapping("siret/sync")
    @ResponseStatus(HttpStatus.OK)
    public void syncCompanies() throws IOException {
        log.info("Start syncing companies...");
        if (CollectionUtils.isEmpty(sirets)) {
            log.warn("No siret was configured !, operation aborted");
        } else {
            companyService.syncCompanies(sirets);
        }
    }
}
