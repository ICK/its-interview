package com.its.interview.interview.client;

import com.its.interview.interview.dto.CompanyResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@FeignClient(name = "company-client", url = "${api.siret.base-url}")
public interface CompanyRestClient {
    @GetMapping(value = "/siret/{siret}")
    CompanyResponseDto findBySiret(@PathVariable("siret") String siret, @RequestHeader Map<String, String> headerMap);

}
