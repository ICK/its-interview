package com.its.interview.interview;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class InterviewApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Value("${application.file.name}")
    private String filename;
    @Value("${application.file.path}")
    private String filepath;

    @Test
    void syncCompanies() throws Exception {
        mockMvc.perform(get("/company/siret/sync")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        Path path = Paths.get(filepath + filename);
        Assertions.assertTrue(Files.exists(path));
    }

}
