FROM openjdk:17-alpine
VOLUME /tmp
COPY target/interview-*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]